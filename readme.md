# How to use: 2019_07_26_OVRInputEvent   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.ovrinputevent":"https://gitlab.com/eloistree/2019_07_26_OVRInputEvent.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.ovrinputevent",                              
  "displayName": "OVRInputEvent",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Listen to events created from the OVR Input.",                         
  "keywords": ["Script","Tool","Oculus Integreation","Events"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    