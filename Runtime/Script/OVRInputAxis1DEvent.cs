﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OVRInputAxis1DEvent : MonoBehaviour
{

    public Range1D[] m_rangeInfo = new Range1D[] {
        new Range1D() { m_description = "Left Click", m_axis=OVRInput.Axis1D.SecondaryIndexTrigger, m_min=0.9f, m_max=1f},
    new Range1D() { m_description = "Heavy steading", m_axis=OVRInput.Axis1D.SecondaryIndexTrigger, m_min=0.5f, m_max=0.9f},
    new Range1D() { m_description = "Light steading", m_axis=OVRInput.Axis1D.SecondaryIndexTrigger, m_min=0.1f, m_max=0.5f} ,
    new Range1D() { m_description = "Not using", m_axis=OVRInput.Axis1D.SecondaryIndexTrigger, m_min=0.0f, m_max=0.1f} };
    
    [System.Serializable]
    public class Range1D
    {
        public string m_description;
        public OVRInput.Axis1D m_axis;
        public float m_min=0.1f;
        public float m_max=1;
        public UnityEvent m_in;
        public UnityEvent m_out;
        [Header("Debug")]
        public float m_currentValue;
        public float m_previousValue;
        public void SetValue(float value) {
            m_currentValue = value;
            if (m_currentValue != m_previousValue) {
                if (IsOut(m_previousValue) && IsIn(m_currentValue) ){
                    m_in.Invoke();
                }
                if (IsIn(m_previousValue) && IsOut(m_currentValue) ){
                    m_out.Invoke();
                }
            }
            m_previousValue = value;
        }

        private bool IsOut(float value)
        {
           return  !IsIn(value);
        }

        private bool IsIn(float value)
        {
            if (m_min > m_max) {
                float val = m_max;
                m_max = m_min;
                m_min = val;
            }
            return value >= m_min && value <= m_max;
        }
    }
    
    void Update()
    {
        for (int i = 0; i < m_rangeInfo.Length; i++)
        {
            UpdateIfGoodAxis(m_rangeInfo[i], OVRInput.Axis1D.PrimaryHandTrigger);
            UpdateIfGoodAxis(m_rangeInfo[i], OVRInput.Axis1D.PrimaryIndexTrigger);
            UpdateIfGoodAxis(m_rangeInfo[i], OVRInput.Axis1D.SecondaryHandTrigger);
            UpdateIfGoodAxis(m_rangeInfo[i], OVRInput.Axis1D.SecondaryIndexTrigger);
        }

    }

    private void UpdateIfGoodAxis(Range1D range, OVRInput.Axis1D axis)
    {
        if (range.m_axis == axis)
            range.SetValue(OVRInput.Get(range.m_axis));
    }
}
