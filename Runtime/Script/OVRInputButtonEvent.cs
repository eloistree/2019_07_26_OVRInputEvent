﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OVRInputButtonEvent : MonoBehaviour
{

    [SerializeField] OVRInput.Button m_button;
    [SerializeField] UnityEvent m_down;
    [SerializeField] UnityEvent m_up;
    [Header("Each Frame if down")]
    [SerializeField] UnityEvent m_update;

    void Update()
    {
        if (OVRInput.GetDown(m_button))
            m_down.Invoke();
        if (OVRInput.Get(m_button))
            m_update.Invoke();

        if (OVRInput.GetUp(m_button))
            m_up.Invoke();

    }
}
